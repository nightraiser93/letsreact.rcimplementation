import {actionTypes} from "./actions";
import roomclassesdata from "../../data/roomclasses";
const initialState = {
    isLoading : true,
    data: [],
    error : null
};
const roomclassReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.REQ_ROOMCLASSES : 
        return {...state, isLoading: false, data:roomclassesdata };
        case actionTypes.ERROR_ROOMCLASSES : 
        return {...state, isLoading: false, error: action.payload}
        default : return state;
    }
}
export default roomclassReducer;