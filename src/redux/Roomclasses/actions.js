export const actionTypes = {
    REQ_ROOMCLASSES : 'REQ_ROOMCLASSES',
    ERROR_ROOMCLASSES : 'ERROR_ROOMCLASSES'
};

export const requestRoomclass = () => {
    return {
        type: actionTypes.REQ_ROOMCLASSES,
    }
};

export const catchRoomclasses = (error) => {
    return {
        type: actionTypes.ERROR_ROOMCLASSES,
        payload: error
    }
}