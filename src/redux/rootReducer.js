import { combineReducers } from "redux";
import roomclassesReducer from "./Roomclasses/reducer";

export default combineReducers({
    roomclassData : roomclassesReducer,
});
