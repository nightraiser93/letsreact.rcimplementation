export default  [
      {
        "propertyId": 889,
        "id": 3780,
        "name": "Queen Tier Deluxe 12",
        "status": "Deleted",
        "roomCount": 2,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3874,
        "name": "Suite 101",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3875,
        "name": "Suite 102",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3876,
        "name": "Suite 201",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3877,
        "name": "Suite 202",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3878,
        "name": "Suite 203",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3879,
        "name": "Suite 204",
        "status": "Inactive",
        "roomCount": 5,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3880,
        "name": "Suite 205",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3881,
        "name": "Suite 206",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3882,
        "name": "Suite 207",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3883,
        "name": "Suite 301",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3884,
        "name": "Suite 302",
        "status": "Inactive",
        "roomCount": 0,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3885,
        "name": "Suite 303",
        "status": "Inactive",
        "roomCount": 0,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 3886,
        "name": "Suite 304",
        "status": "Inactive",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 10666,
        "name": "Run of the House",
        "status": "Active",
        "roomCount": 0,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 15955,
        "name": "RunOfHouse",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 18516,
        "name": "Best Available King Deluxe",
        "status": "Inactive",
        "roomCount": 0,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 18517,
        "name": "Best Available Queen Deluxe",
        "status": "Inactive",
        "roomCount": 2,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 26315,
        "name": "Best Available at Check-In",
        "status": "Active",
        "roomCount": 5,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30335,
        "name": "Surya11",
        "status": "Active",
        "roomCount": 30,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30336,
        "name": "SR11",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30337,
        "name": "Steve Test Room Class",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30338,
        "name": "Steves Test ",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30339,
        "name": "surya test",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30340,
        "name": "Room11",
        "status": "Inactive",
        "roomCount": 7,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30382,
        "name": "New Room",
        "status": "Inactive",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30425,
        "name": "Rm1",
        "status": "Active",
        "roomCount": 3,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30487,
        "name": "a1",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30600,
        "name": "TestRoomS",
        "status": "Deleted",
        "roomCount": 6,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30730,
        "name": "Delete this",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30731,
        "name": "Delete this",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30783,
        "name": "Some",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30787,
        "name": "Delete me",
        "status": "Deleted",
        "roomCount": 10,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30795,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 4,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30796,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30797,
        "name": "Best Available at Check-Inn",
        "status": "Deleted",
        "roomCount": 6,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30798,
        "name": "Best Available at Check-Inn3",
        "status": "Deleted",
        "roomCount": 6,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30799,
        "name": "Best Available at Check-Inn 3",
        "status": "Deleted",
        "roomCount": 6,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30800,
        "name": "Best Available at Check-Inn 3",
        "status": "Deleted",
        "roomCount": 6,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30802,
        "name": "Test 1",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30803,
        "name": "Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30804,
        "name": "AAA1",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30805,
        "name": "AAA3",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30806,
        "name": "ABC2",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30807,
        "name": "ABC 1",
        "status": "Deleted",
        "roomCount": 10,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30808,
        "name": "ABC 2",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30809,
        "name": "ABC 3",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30810,
        "name": "ABC 2",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30811,
        "name": "ABC4",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30812,
        "name": "Delete me",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30829,
        "name": "Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30830,
        "name": "Room 237",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30831,
        "name": "Room 237",
        "status": "Deleted",
        "roomCount": 3,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30840,
        "name": "Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30855,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 2,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30881,
        "name": "Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 30928,
        "name": "1room",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30929,
        "name": "A Room 237",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30931,
        "name": "A Room 237 2",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30938,
        "name": "A Room 237 4",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30943,
        "name": "A Room 237 6",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30945,
        "name": "A Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30946,
        "name": "A Room 237 7",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30947,
        "name": "A Room 237 10",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30949,
        "name": "A Room 2",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30951,
        "name": "A Room 5",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30952,
        "name": "A Room 6",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30953,
        "name": "A Room 10",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30958,
        "name": "A Room 12",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 30959,
        "name": "A Room 237 ",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31005,
        "name": "A 237",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31006,
        "name": "A 238",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31007,
        "name": "A 239",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31008,
        "name": "A 300",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31016,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 4,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31017,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31021,
        "name": "237 AB",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31022,
        "name": "237 A",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31023,
        "name": "237 A",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31024,
        "name": "237 A",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31025,
        "name": "Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31026,
        "name": "237 B",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31027,
        "name": "237 E",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31028,
        "name": "237 AB",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31029,
        "name": "237 BC",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31030,
        "name": "237 C",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31031,
        "name": "237 DE",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31032,
        "name": "237 AB",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31033,
        "name": "237 CDE",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31034,
        "name": "237 ABC",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31036,
        "name": "237 Room ABC",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31044,
        "name": "A Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31045,
        "name": "A Room 237",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31046,
        "name": "A Room 237 2",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31047,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31049,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31051,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31052,
        "name": "A Room 237 3",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31053,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31057,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31058,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31059,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31060,
        "name": "237 Room 2",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31062,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31090,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31108,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31121,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31122,
        "name": "Mike Test Update",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31249,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31260,
        "name": "239 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31261,
        "name": "240 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31262,
        "name": "241 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31263,
        "name": "242 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31264,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31265,
        "name": "401 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31266,
        "name": "222",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31267,
        "name": "222 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31268,
        "name": "2121",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31269,
        "name": "234 Room ",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31270,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31271,
        "name": "555 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31273,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31277,
        "name": "Test Room SK",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31279,
        "name": "Test SK 2",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31280,
        "name": "Test sk 3",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31284,
        "name": "444 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31285,
        "name": "333 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31286,
        "name": "333 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31287,
        "name": "333 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31289,
        "name": "455 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31290,
        "name": "443 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31293,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31294,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31295,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31296,
        "name": "222 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31297,
        "name": "434 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31298,
        "name": "999 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31300,
        "name": "888 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31301,
        "name": "555 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31302,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31303,
        "name": "143 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31305,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31312,
        "name": "New Room Class - 1",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31313,
        "name": "Test fail ",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31323,
        "name": "Test Mike",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31324,
        "name": "122 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31325,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31326,
        "name": "237 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31327,
        "name": "232 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31328,
        "name": "555 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31332,
        "name": "Test room",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31333,
        "name": "Test RCA",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31334,
        "name": "111 Room",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31335,
        "name": "333 Room",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31336,
        "name": "Mike Virtual ",
        "status": "Active",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31337,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31338,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31339,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 0,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31341,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31343,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31344,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31345,
        "name": "222 Room",
        "status": "Deleted",
        "roomCount": 3,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31346,
        "name": "555 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31347,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 3,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31348,
        "name": "999 Room",
        "status": "Deleted",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31349,
        "name": "444 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31350,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31351,
        "name": "222 Room",
        "status": "Deleted",
        "roomCount": 0,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31353,
        "name": "666 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31354,
        "name": "to test",
        "status": "Active",
        "roomCount": 6,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31355,
        "name": "to delete",
        "status": "Deleted",
        "roomCount": 2,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31356,
        "name": "Kishan",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31357,
        "name": "Krish",
        "status": "Active",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31360,
        "name": "jink",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31362,
        "name": "SD1",
        "status": "Active",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31363,
        "name": "333 Room",
        "status": "Deleted",
        "roomCount": 6,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31364,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31369,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31371,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31372,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31374,
        "name": "123 Test Room ",
        "status": "Deleted",
        "roomCount": 19,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31375,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 9,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31380,
        "name": "556 Room",
        "status": "Deleted",
        "roomCount": 7,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31384,
        "name": "123 Room",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31388,
        "name": "888 Rooms",
        "status": "Deleted",
        "roomCount": 11,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31389,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 4,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31390,
        "name": "777 Room",
        "status": "Deleted",
        "roomCount": 7,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31391,
        "name": "333 Room",
        "status": "Deleted",
        "roomCount": 5,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31392,
        "name": "Srujitha",
        "status": "Active",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31393,
        "name": "Sru_Inn",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31394,
        "name": "murali",
        "status": "Active",
        "roomCount": 1,
        "published": "Yes"
      },
      {
        "propertyId": 889,
        "id": 31395,
        "name": "123 Test",
        "status": "Deleted",
        "roomCount": 1,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31396,
        "name": "237 Room",
        "status": "Active",
        "roomCount": 2,
        "published": "No"
      },
      {
        "propertyId": 889,
        "id": 31400,
        "name": "666 Room",
        "status": "Active",
        "roomCount": 1,
        "published": "No"
      }
    ]