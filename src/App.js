import React, { Component } from 'react';
import {Provider} from "react-redux";

import Header from "./components/Header/Header";
import Roomclasses from "./components/Roomclasses";
import 'antd/dist/antd.css';
import configureStore from "./store";
const store = configureStore({});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <div>
        <Header />
        <Roomclasses />
      </div>
      </Provider>
    );
  }
}

export default App;
