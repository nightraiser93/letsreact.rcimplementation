import { connect } from "react-redux";
import {bindActionCreators } from "redux";
import Roomclasses from "./Roomclasses";
import {requestRoomclass} from "../../redux/Roomclasses/actions";
const mapStoreToProps = (store) => {
    return {
        data : store.roomclassData.data,
        isLoading: store.roomclassData.isLoading,
        error: store.roomclassData.error
    }
}

const mapActionsToProps = dispatch => bindActionCreators({
    requestRoomclass
},dispatch);
export default connect(mapStoreToProps, mapActionsToProps)(Roomclasses);