import React, { Component } from 'react'
import { Table, Select } from "antd";

const Option = Select.Option;

const columns = [{
    title: 'Room class Name',
    dataIndex: 'name',
  },
  {
      title: 'Rooms',
      dataIndex: 'roomCount'
  },
  {
    title: 'Status',
    dataIndex: 'status'
},
{
    title: 'Published',
    dataIndex: 'published'
}];


export default class Roomclasses extends Component {  
  
 state = {
     roomclassesList: this.props.data
 };
 componentDidMount() {
    this.props.requestRoomclass();
 }
 componentDidUpdate(prevProps) {
    
    if(prevProps.isLoading ===true && this.props.isLoading === false) {
        this.setState({roomclassesList: this.props.data});
    }
 }
  handleStatusChange = (value) => {
    const roomclassesData = this.props.data;
    if(value !== "all") {
        const filteredData = roomclassesData.filter((roomclass) => roomclass.status === value);
        this.setState({roomclassesList:filteredData});
    }else{
        this.setState({roomclassesList:roomclassesData})
    }
  }  
  render() {
    return (
      <div style={{padding:"10px"}}>
        <Select onChange={this.handleStatusChange} defaultValue="all">
            <Option value="all">All</Option>
            <Option value="Active">Active</Option>
            <Option value="Inactive">Inactive</Option>
            <Option value="Obsolete">Obsolete</Option>
        </Select>
        <Table dataSource={this.state.roomclassesList} columns={columns} />
      </div>
    )
  }
}
