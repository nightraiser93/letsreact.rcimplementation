import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div style={{backgroundColor:"#165d93", height:"60px", textAlign:'center',color:'#fff'}}>
        <h2>innRoad</h2>
      </div>
    )
  }
}
